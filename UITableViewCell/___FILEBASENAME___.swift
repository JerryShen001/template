//___FILEHEADER___

import UIKit

class ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_cocoaTouchSubclass___ {
    deinit {
        print("\(type(of: self)) is deinit")
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - private 成员
    
}
// MARK: - internal 方法
extension ___FILEBASENAMEASIDENTIFIER___{
    
}
// MARK: - private 方法
extension ___FILEBASENAMEASIDENTIFIER___{
    /// 添加UI或者设置UI属性等操作
    private func setupUI(){
        selectionStyle = .none
    }
    /// 设置UI约束
    private func makeConstraints(){
        
    }
}
