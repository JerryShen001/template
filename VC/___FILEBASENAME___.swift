//___FILEHEADER___

import UIKit

class ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_cocoaTouchSubclass___ {
    deinit {
        print("\(type(of: self)) is deinit")
    }
    init() {
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - 生命周期
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        makeConstraints()
        
    }
    
    // MARK: - private 成员

}
// MARK: - internal 方法
extension ___FILEBASENAMEASIDENTIFIER___ {
    
}
// MARK: - private 方法
extension ___FILEBASENAMEASIDENTIFIER___{
    /// 添加UI或者设置UI属性等操作
    private func setupUI() {
        
    }
    /// 设置UI约束
    private func makeConstraints() {
        
    }
}
